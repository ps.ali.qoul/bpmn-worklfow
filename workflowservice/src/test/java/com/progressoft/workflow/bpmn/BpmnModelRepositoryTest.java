package com.progressoft.workflow.bpmn;

import com.progressoft.workflow.bpmn.BpmnModelRepository.ModelLoadingError;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BpmnModelRepositoryTest {

    private final BpmnModelRepository repository = new BpmnModelRepository();

    @Test
    void givenWorkflowDoesNotExistThenLoadingFails() {
        assertThrows(ModelLoadingError.class, () -> repository.load("not existing"));
    }

    @Test
    void givenWorkflowExistThenLoadingSucceed() {
        BpmnModel model = repository.load("MAKER_CHECKER");
        assertEquals("MAKER_CHECKER", model.getName());
        assertNotNull(model.get());
    }
}