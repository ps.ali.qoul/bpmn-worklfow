package com.progressoft.workflow.bpmn.contract;

public interface ModelRepository<T extends BpmnWorkflowModel<?>> {

    T load(String name);
}
