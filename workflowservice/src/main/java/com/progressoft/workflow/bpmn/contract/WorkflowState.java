package com.progressoft.workflow.bpmn.contract;

import lombok.Getter;

@Getter
public class WorkflowState {

    private final String workflow;
    private final String currentStepId;
    private final String action;
    private final Object item;

    public WorkflowState(String workflow, String currentStepId, String action, Object item) {
        this.workflow = workflow;
        this.currentStepId = currentStepId;
        this.action = action;
        this.item = item;
    }
}
